|Title|   Create Project    |
|-------|-----------|
|Value Statement | As a leader, I want to be able to create projects so I can manage work progress |
|Acceptance Criteria|**Acceptance Citerion:** <br>Given that I am a leader <br>When I create project<br> Then I can manage members and project|
|Defintion of Done|- Acceptance Criteria Met<br> - Test Cases Passed<br> - Package Into Specific Folders<br> - Synchronize Folders Between Devices<br> - Show Topic List<br>  - Code Reviewed<br> - Product Owner Accepts User Story
|Owner|Tran Quoc Trung|
|Iteration|Unsheduled|
|Estimate|5 Points|

|Title|   Edit Project   |
|-------|-----------|
|Value Statement | As a leader, I want the ability to modify the details of an existing project within the project management system to keep project information accurate and up-to-date. |
|Acceptance Criteria|**Acceptance Citerion:** <br>Given I click on the "Edit" option for the project <br>When the project's details are displayed for editing<br>Then I should be able to modify the project name, start date, end date, assigned team members, description, and status.|
|Defintion of Done|- Acceptance Criteria Met<br> - Test Cases Passed<br> - Package Into Specific Folders<br> - Synchronize Folders Between Devices<br> - Show Topic List<br>  - Code Reviewed<br> - Product Owner Accepts User Story
|Owner|Tran Quoc Trung|
|Iteration|Unsheduled|
|Estimate|5 Points|

|Title|   Remove Project   |
|-------|-----------|
|Value Statement | As a leader, I want to be able to delete that project when it is no longer in use |
|Acceptance Criteria|**Acceptance Citerion:**<br> GIven that i already have an project <br> When I attempt to delete it through "Delete" option <br> Then the project should be permanently deleted from the project management system|
|Defintion of Done|- Acceptance Criteria Met<br> - Test Cases Passed<br> - Package Into Specific Folders<br> - Synchronize Folders Between Devices<br> - Show Topic List<br>  - Code Reviewed<br> - Product Owner Accepts User Story
|Owner|Nguyen Hung Phat|
|Iteration|Unsheduled|
|Estimate|5 Points|
